jQuery(document).ready(function(){
		$(".right_nav").hide();

    $('#toggle-menu').click(function () {
    	$(".right_nav").toggle("slide", { direction: "right" }, 1000);

		$(this).toggleClass('em-icon--active em-icon-grey');
    });


////////////////////////////////////
// Translate To Section When Click
////////////////////////////////////

$("#intro-anchor").click(function(){
	$("#slides").css("transform","translate(0%, 0%)");
});
$("#anchor1").click(function(){
	$("#slides").css("transform","translate(0%, -50%)");
	$(".about-img").css({"opacity":"1","transition-delay":"1s, 0.8s, 0s"});
});
$("#anchor2").click(function(){
	$("#slides").css("transform","translate(-50%, -50%)");
});
$("#anchor3").click(function(){
	$("#slides").css("transform","translate(-50%, 0%)");
});


////////////////////////////////////
// Scroll to Section
///////////////////////////////////

  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });

});