jQuery(document).ready(function(){





// Init ScrollMagic
var controller = new ScrollMagic.Controller();

//=====================================================
//Insert From Left
//=====================================================

$("#moveship").each(function(){

var fromLeftTimeline = new TimelineMax();
var fromLeftFrom = TweenMax.from($(this), 1, {
    x: 1000
});
var fromLeftTo = TweenMax.to($(this), 1, {
    x: 0
});
fromLeftTimeline
    .add(fromLeftFrom)
    .add(fromLeftTo);
new ScrollMagic.Scene({
        triggerElement: this,
        duration:500,
        triggerHook:0.6,
        reverse:true,
        offset:0
})
    .setTween(fromLeftTimeline)
    .duration(400)
/*    .addIndicators({name:'left_slide',colorStart:'yellow',colorEnd:'yellow'})*/
   .reverse(true)

    .addTo(controller);

});




//=================================================
//Fly in from the Bottom
//=================================================


$(".em-from-bottom").each(function(){

var fromBottomTimeline = new TimelineMax();
var fromBottomFrom = TweenMax.from($(this), 1, {
    y: 100,
    autoAlpha: 0
});
var fromBottomTo = TweenMax.to($(this), 1, {
    y: 0,
    autoAlpha: 1
});
fromBottomTimeline
    .add(fromBottomFrom)
    .add(fromBottomTo);
 
new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook:0.9,
        reverse:true,
        offset:0
    })
    .setTween(fromBottomTimeline)
    .duration(400)
    //    .reverse(false)
/*      .addIndicators({name:'From Bot',colorTrigger:'black',colorStart:'black',colorEnd:'black'})*/
    .addTo(controller);  

});






// ====================================================
//Fade in
//=====================================================

$(".em-fade-in").each(function(){

var fadeInTimeline = new TimelineMax();
var fadeInFrom = TweenMax.from($(this), 1, {
    autoAlpha: 0
});
var fadeInTo = TweenMax.to($(this), 1, {
    autoAlpha: 1
});
fadeInTimeline
    .add(fadeInFrom)
    .add(fadeInTo);
 
new ScrollMagic.Scene({
        triggerElement: this,
        duration:500,
        triggerHook:0.8,
        reverse:true,
        offset:0
    })
    .setTween(fadeInTimeline)
    .duration(400)
    //    .reverse(false)
/*    .addIndicators({name:'fade',colorTrigger:'violet',colorStart:'violet',colorEnd:'violet'})*/
    .addTo(controller);

});

//============================================
// Fade From Left
//=============================================


$(".em-fade-left").each(function(){

var fromLeftTimeline = new TimelineMax();
var fromLeftFrom = TweenMax.from($(this), 1, {
    x: -300,
    autoAlpha: 0,
    left: "+=50",  

});
var fromLeftTo = TweenMax.to($(this), 1, {
    x: 0,
    autoAlpha: 1

});
fromLeftTimeline
    .add(fromLeftFrom)
    .add(fromLeftTo);
new ScrollMagic.Scene({
        triggerElement:this,
        duration:500,
        triggerHook:0.8,
        reverse:true,
        offset:0
})
    .setTween(fromLeftTimeline)
    .duration(400)
/*    .addIndicators({name:'left_slide',colorStart:'yellow',colorEnd:'yellow'})*/
   .reverse(true)

    .addTo(controller);

});


//============================================
// Fade From Right
//=============================================

$(".em-fade-right").each(function(){

var fromLeftTimeline = new TimelineMax();
var fromLeftFrom = TweenMax.from($(this), 1, {
    x: 300,
    autoAlpha: 0

});
var fromLeftTo = TweenMax.to($(this), 1, {
    x: 0,
    autoAlpha: 1   
});
fromLeftTimeline
    .add(fromLeftFrom)
    .add(fromLeftTo);
new ScrollMagic.Scene({
        triggerElement: this,
        duration:500,
        triggerHook:0.8,
        reverse:true,
        offset:0
})
    .setTween(fromLeftTimeline)
    .duration(400)
/*    .addIndicators({name:'left_slide',colorStart:'yellow',colorEnd:'yellow'})*/
   .reverse(true)

    .addTo(controller);
});


//============================================
// From Left
//=============================================



$(".em-left").each(function(){

var fromLeftTimeline = new TimelineMax();
var fromLeftFrom = TweenMax.from($(this), 1, {
    x: -350,


});
var fromLeftTo = TweenMax.to($(this), 1, {
    x: 0,

});
fromLeftTimeline
    .add(fromLeftFrom)
    .add(fromLeftTo);
new ScrollMagic.Scene({
        triggerElement: this,
        duration:500,
        triggerHook:0.5,
        reverse:true,
        offset:0
})
    .setTween(fromLeftTimeline)
    .duration(400)
/*    .addIndicators({name:'left_slide',colorStart:'yellow',colorEnd:'yellow'})*/
   .reverse(true)

    .addTo(controller);
});



//============================================
// From Right
//=============================================



$(".em-right").each(function(){

var fromLeftTimeline = new TimelineMax();
var fromLeftFrom = TweenMax.from($(this), 1, {
    x: 350,

});
var fromLeftTo = TweenMax.to($(this), 1, {
    x: 0,

});
fromLeftTimeline
    .add(fromLeftFrom)
    .add(fromLeftTo);
new ScrollMagic.Scene({
        triggerElement: this,
        duration:500,
        triggerHook:0.5,
        reverse:true,
        offset:0
})
    .setTween(fromLeftTimeline)
    .duration(400)
/*    .addIndicators({name:'left_slide',colorStart:'yellow',colorEnd:'yellow'})*/
   .reverse(true)

    .addTo(controller);
});








//============================================
// From Left Slide 3
//=============================================



$(".em-left-slide-3").each(function(){

var fromBottomTimeline = new TimelineMax();
var fromBottomFrom = TweenMax.from($(this), 1, {
    y: 200,
    autoAlpha: 0
});
var fromBottomTo = TweenMax.to($(this), 1, {
    y: 0,
    autoAlpha: 1
});
fromBottomTimeline
    .add(fromBottomFrom)
    .add(fromBottomTo);
 
new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook:0.3,
        reverse:true,
        offset:0
    })
    .setTween(fromBottomTimeline)
    .duration(400)
    //    .reverse(false)
/*      .addIndicators({name:'From Bot',colorTrigger:'black',colorStart:'black',colorEnd:'black'})*/
    .addTo(controller);  

});



//============================================
// From Right Slide 3
//=============================================



$(".em-right-slide-3").each(function(){

var fromBottomTimeline = new TimelineMax();
var fromBottomFrom = TweenMax.from($(this), 1, {
    y: -200,
    autoAlpha: 0
});
var fromBottomTo = TweenMax.to($(this), 1, {
    y: 0,
    autoAlpha: 1
});
fromBottomTimeline
    .add(fromBottomFrom)
    .add(fromBottomTo);
 
new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook:0.3,
        reverse:true,
        offset:0
    })
    .setTween(fromBottomTimeline)
    .duration(400)
    //    .reverse(false)
/*      .addIndicators({name:'From Bot',colorTrigger:'black',colorStart:'black',colorEnd:'black'})*/
    .addTo(controller);  
});



//=================================
// PIN 
//=======================================


    var pinIntroScene = new ScrollMagic.Scene({
    triggerElement:'#sticky1',
    triggerHook:0,
    duration:'30%'
    })

    .setPin('#sticky1',{pushFollowers: false})
    .addTo(controller);





});



