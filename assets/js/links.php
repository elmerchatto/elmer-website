<?php
  function oc_enqueue_scripts() {
    wp_enqueue_style( 'style-1', get_stylesheet_directory_uri()  . 'css/scroll.css', array(), '1.0', 'all' );
    wp_enqueue_script( 'script-1', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js');
    wp_enqueue_script( 'script-2', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js');
    wp_enqueue_script( 'script-3', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js');
    wp_enqueue_script( 'script-4', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js');
    wp_enqueue_script( 'script-5', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js');
    wp_enqueue_script( 'script-6', get_stylesheet_directory_uri().'/js/scroll.js', array('jquery'), '1.0', true);
    wp_enqueue_script( 'script-7', get_stylesheet_directory_uri().'/js/turnBox.js', array('jquery'), '1.0', true);
    }
?>